export class CreateItemsDto{

    readonly id:string;
    readonly name: string;
    readonly description: string;
    readonly qty: number;

}