import { Controller, Get,Post,Put,Delete,Body,Req,Res,Param } from '@nestjs/common';
import {CreateItemsDto} from './dto/create-item.dto';
import {Request,Response} from 'express';
import {ItemsService} from './items.service';
import {Item} from './interfaces/item.interface';

@Controller('items')
export class ItemsController {

    constructor(private readonly itemsService: ItemsService){}

    @Get()
    async findAll():Promise<Item[] | string | any>{
        // return res.status(200).json({message:"First Nest"})
        return this.itemsService.findAll()
    }

    @Get(':id')
    async findOne(@Param('id') id): Promise<Item>{
        return this.itemsService.findOne(id)
    }

    @Post()
    create(@Body() createItemDto:CreateItemsDto):Promise<Item | String | any> {
        console.log("DTO ->");
        console.log(createItemDto);
        return this.itemsService.create(createItemDto);
    }

    @Delete(':id')
    delete(@Param('id') id):Promise<Item | String | any> {
        return this.itemsService.delete(id);
    }

    @Put(':id')
    update(@Body() updateItemDto:CreateItemsDto,@Param('id') id):Promise<Item | String | any> {
        return this.itemsService.update(id,updateItemDto);
    }

}
