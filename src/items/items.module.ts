import { Module } from '@nestjs/common';
import { MongooseModule as mm } from '@nestjs/mongoose';
import { ItemsController } from './items.controller';
import { ItemsService } from './items.service';
import {ItemSchema} from './schemas/item.schema';


@Module({
  imports: [mm.forFeature([{name:"Nestfirst",schema:ItemSchema}])],
  controllers: [ItemsController],
  providers: [ItemsService],
})

export class ItemsModule {}