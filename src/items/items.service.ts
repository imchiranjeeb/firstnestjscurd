import { Injectable } from '@nestjs/common';
import {Item} from './interfaces/item.interface';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';
// import {NestFirst} from './items.module'


@Injectable()
export class ItemsService {

    constructor(@InjectModel('Nestfirst') private readonly itemModel: Model<Item>){}
    

    async findAll(): Promise<Item[] | String | any> {
        const result = await this.itemModel.find();

        if(result.length === 0) {
            return "No Items Found.";
        }
        return result
    }

    async findOne(id:string):Promise<Item>{
        return await this.itemModel.findOne({_id:id});
    }

    async create(item:Item):Promise<Item | String | any>{
        
        if(!item.name || !item.qty || !item.description){
            return "All Fields are Mandatory"
        }
        const newItem = new this.itemModel(item);
        return await newItem.save();
    }

    async delete(id: string):Promise<Item | String | any>{
        const findItem = await this.itemModel.findOne({_id:id});
        if (!findItem){
            return "Not Able to Find The Job";
        }
        const removedJob = findItem.remove();
        return "Job Removed";
    }

    async update(id: string,item:Item):Promise<Item | String | any>{
        const updateItem = await this.itemModel.findByIdAndUpdate(id,item,{new:true});
        if(!updateItem){
            return "No Items Found"
        }
        return updateItem;
    }
}
