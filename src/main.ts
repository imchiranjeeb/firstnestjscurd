import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {ItemsModule} from './items/items.module'

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(5006);
}


bootstrap();
